
import './App.css'

function App() {


  return (
  
    <>

  <div>
    <header>
      <div classname="container">
        <span id="logofont">Tigris</span>
        <button id="logo">
          <span classname="material-symbols-outlined">pets</span>
        </button>
        <nav>
          <ul id="navlist">
            <li>
              <a href="#">Home</a>
            </li>
            <li>
              <a href="#">About</a>
            </li>
            <li>
              <a href="#">Services</a>
            </li>
            <li>
              <a href="#">Contact</a>
            </li>
          </ul>
        </nav>
      </div>
    </header>
    <main>
      <section>
        <div classname="container">
          <h1>A Sanctuary for Wildlife Conservation</h1>
          <p>
            Welcome to Tigris , where untamed beauty meets dedicated
            conservation. Our sanctuary is a haven for majestic tigers and
            diverse wildlife, nestled in pristine landscapes. Explore the
            delicate balance of nature through our virtual journey, and join us
            in preserving the legacy of these magnificent creatures for
            generations to come.
          </p>
          <div classname="btn-holder">
            <a classname="btn-primary" href="#">
              Book a Safari
            </a>
            <a classname="btn-secondary" href="#">
              Contact Us
            </a>
          </div>
        </div>
      </section>
      <section id="about">
        <div classname="container">
          <h2>A Commitment to Conservation and Biodiversity</h2>
          <p>
            Our commitment to conservation and biodiversity transcends a mere
            mission; it is a holistic philosophy guiding every facet of [Name of
            Tiger Reserve]. With a deep understanding of the interconnectedness
            of ecosystems, we implement sustainable practices and innovative
            initiatives. Beyond protecting charismatic species, our efforts
            extend to preserving the intricate relationships among plants,
            animals, and their environments. Engaging local communities fosters
            a shared responsibility for environmental stewardship. Through
            ongoing research and adaptation, we navigate the evolving challenges
            posed by climate change and human impact, ensuring a resilient and
            biodiverse landscape for current and future generations to marvel at
            and cherish. Together, we forge a legacy of harmonious coexistence
            with nature.
          </p>
        </div>
      </section>
      <section>
        <div classname="container">
          <h2>Roars and Insights</h2>
          <div classname="blog-list">
            <article classname="blog">
              <img src="./donnie-ray-crisp-66zrT0dJ7Mc-unsplash.jpg" alt="" />
              <h3>Conservating the majestic big cats</h3>
              <p>
                Explore the critical endeavors in conserving majestic big cats.
                From habitat protection to anti-poaching initiatives, delve into
                the dedicated efforts securing the survival of these iconic
                predators.
              </p>
            </article>
            <article classname="blog">
              <img src="./photo-1561731216-c3a4d99437d5.avif" alt="" />
              <h3>Unveiling the Enigmatic Lives of Majestic Tigers</h3>
              <p>
                Embark on a riveting journey through the dense jungles with
                "Unveiling the Enigmatic Lives of Majestic Tigers." Discover the
                untold stories, challenges, and triumphs of these awe-inspiring
                creatures.
              </p>
            </article>
            <article classname="blog">
              <img src="./photo-1573476492143-70270ca42661.avif" alt="" />
              <h3>
                A Glimpse into the Elegance and Power of Tigers in the Wild
              </h3>
              <p>
                "Explore the untamed realm where raw power meets graceful
                elegance. Dive into the wild to witness the majestic allure of
                tigers."
              </p>
            </article>
            <article classname="blog">
              <img src="./photo-1615474286632-e31ac3633d58.avif" alt="" />
              <h3>Tigris and the Ecological Harmony They Uphold</h3>
              <p>
                "Tigris, the silent guardians of our forests, play a pivotal
                role in upholding ecological harmony. Discover their profound
                impact on biodiversity and the delicate balance of nature they
                preserve."
              </p>
            </article>
          </div>
        </div>
      </section>
    </main>
    <footer></footer>
  </div>



    </>
  )
}

export default App
